- name: Past Due InfraDev Issues
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Measures the number of past due infradev issues by severity.
  target: At or below 5 issues
  org: Development Department
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - We are now below 5. We did have a small spike earlier this quarter which was addressed.
    - We will continue to work to reduce this 0.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/TopEngineeringMetrics/PastdueInfradevIssuesbySeverity
- name: Past Due Security Issues
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Measures the number of past due security issues by severity. This is filtered down to issues with either a stage or group label.
  target: At or below 20 issues
  org: Development Department
  is_key: true
  public: true
  health:
    level: 2
    reasons:
    - Security issues continue to be an area of focus as requirements have become more stringent.
    - Large increases due to new evaluation plus some automation that wasn't addressing issues properly (composition analysis).  We are in process of remediating that.
    - The due date of issues is set to the latest date that can be included in a release that will go out before the SLA expires
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/TopEngineeringMetrics/PastdueSecurityIssuesbySeverity
- name: MR Rate
  base_path: "/handbook/engineering/development/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-mr-rate"
  definition: Development Department MR Rate is a performance indicator showing how many changes the Development Department implements directly in the GitLab product.
    This is the ratio of product MRs to the number of team members in the Development Department.
    It's important because it shows us how productivity of our projects have changed over time.
    The full definition of MR Rate is linked in the url section.
  target: Above 12 MRs per month
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are seeing positive trends with MR Rate (April) we expect similar improvements months coming.
  urls:
    - "/handbook/engineering/metrics/#merge-request-rate"
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/DevelopmentEmbeddedDashboard/DevelopmentMRRate
- name: UX Debt
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: See <a href="https://about.gitlab.com/handbook/product/ux/performance-indicators/#ux-debt"> UX Debt</a> for the definition.  We include this as part of the development PIs since we need to help to positively affect change of this metric.
  target: Below 50 open "ux debt" issues
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - Currently following UX guidance for health where we are above target.
    - This metric may change as our usability strategy is being reviewed.
  urls:
    - "/handbook/product/ux/performance-indicators/#ux-debt"
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OpenUXDebtIssuesOverTime/OpenUXDebtIssuesOverTime
- name: Largest Contentful Paint (LCP)
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Largest Contentful Paint (LCP) is an important, user-centric metric for measuring the largest load speed visible on the web page. To provide a good user experience on GitLab.com, we strive to have the LCP occur within the first few seconds of the page starting to load. This LCP metric is reporting on our <a href="https://gitlab.com/ddevault/scdoc"> Projects Home Page</a>. LCP data comes from the <a href="https://gitlab.com/gitlab-data/analytics/tree/master/extract/graphite/"> Graphite database</a>. A <a href="https://dashboards.gitlab.net/d/performance-comparison/github-gitlab-performance?orgId=1">Grafana dashboard</a> is available to compare LCP of GitLab.com versus GitHub.com on key pages, in additon to a <a href="https://forgeperf.org/"> third party site</a> with a broader comparison. LCP p90 data is captured every 4 hours and we report on the latest value each day.
  target: Below 2500ms at the 90th percentile
  org: Development Department
  is_key: true
  health:
    level: 3
    reasons:
    - We are currently below targets consistently other than a small number of day outliers.
  tableau_data:
    charts:
     - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/LargestContentfulPaintLCPp90ofProjectHomepage/Sheet1
- name: Development Handbook MR Rate
  base_path: "/handbook/engineering/development/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-merge-request-rate"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching `/source/handbook/engineering/development/**` over time. The calculation for the monthly  handbook MR rate is the number of handbook updates divided by the number of team members in the Development Department for a given month.
  target: At or above 0.5
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    -  We need to look into new ways to enhance our handbook experience if we want to hit target.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/HandbookMRRate/HandbookMRRateDashboard
        parameters:
          - field: Department/Division
            value: Development
- name: Development Department Discretionary Bonus Rate
  base_path: "/handbook/engineering/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-discretionary-bonus-rate"
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined. This metric definition is taken from the <a href="/handbook/people-group/people-success-performance-indicators/#discretionary-bonuses">People Success Discretionary Bonuses KPI</a>.
  target: at or above 10%
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
      - We have been consistently below 10% goal for the last 6 months.
      - We have given more discretionary bonuses YoY.
      - We will discuss having another review to encourage more recognition.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/R3DiscretionaryBonusRate
        filters:
          - field: DPT Modified Department
            value: Development
- name: Open MR Review Time (OMRT)
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: We want to be more intuitive with calculating how long it takes an MR in review state. Open MR Review Time (OMRT) measures the median time of all open MRs in review as of a specific date. In other words, on any given day, we calculate the number of open MRs in review and median time in review state for those MRs at that point in time. MRs are considered in review at the point when a review is requested on an MR.
  target: At or below 21
  org: Development Department
  is_key: true
  health:
    level: 2
    reasons:
    - Have broken out by community vs. company.   We are keeping company review times stable.
    - Summer months have seen a slight increase which should be monitored moving forward.
    - Looking at reducing the tail (stale MRs) is still an option to be explored.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OMRT/DailyOpenMRTime
- name: Development Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-team-member-retention"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab. Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: Development Department
  is_key: true
  public: false
  health:
    level: 3
    reasons:
    - above target and monitoring as retention remains a concern
  urls:
    - "https://10az.online.tableau.com/#/site/gitlab/views/N5AttritionDashboard/AttritionDashboard?:iid=1"
- name: Development Average Age of Open Positions
  base_path: "/handbook/engineering/development/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-vacancy-time-to-fill"
  definition: Measures the average time job openings take from open to close. This metric includes sourcing time of candidates compared to Time to Hire or Time to Offer Accept which only measures the time from when a candidate applies to when they accept.
  target: at or below 50 days
  org: Development Department
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - Open age has been stable the past quarter.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/ReqAging
        filters:
          - field: DPT Modified Department
            value: Development
- name: Average PTO per Development Team Member
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: This shows the average number of PTO days taken per Development Team Member. It is the ratio of PTO days taken (vacation, sick leave, public holidays, Family & Friends days, etc) to the number of team members in the Development Department each month. Looking at the average number of PTO days over time helps us understand increases or decreases in efficiency and ensure that team members are taking time off to keep a healthy work/life balance.
  target: TBD
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - Need to monitor on a monthly basis
    - PTO has stabilized in the past few months.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/PTOTrend
        filters:
          - field: DPT Modified Department
            value: Development
- name: Escape Rate Over Time
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: This shows the rate that bugs are created. It is the ratio of opened bugs to the number of MRs merged. As an example, an escape rate of 10% indicates that, on average, for every 10 MRs merged we will see 1 bug opened. Looking at the escape rate helps us understand the quality of the MRs we are merging.
  target: Currently no target is set for this metric.  We need to establish a baseline and consider the right balance between velocity and quality.
  org: Development Department
  is_key: false
  health:
    level: 0
    reasons:
    - Spike in October is due to the increased scrutiny as a part of the FedRamp project.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/IssueTypesDetail/EscapeRate
- name: Backend Unit Test Coverage
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: BE Unit Test coverage shows the unit test coverage of our code base.  As
    an example 95% represents that 95% of the LOC in our BE software is unit tested.  It’s
    important as it shows how much code is tested early in the development process.
  target: Above 95%
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - This metric's threshold for action is around 95%.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/SourceCodeKPIs/BackendCoverage
  urls:
  - https://app.periscopedata.com/app/gitlab/475029/Source-Code-KPI's
- name: CVE issue to update
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Measurement of time CVE being issued to our product being updated.
  target: 7 days (until further data is provided)
  org: Development Department
  health:
    level: 3
    reasons:
    - We are measuring in Sisense/Periscope and are currently under our targeted threshold
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/issues/5936
  sisense_data:
    chart: 7692993
    dashboard: 588449
    embed: v2
- name: Frontend Unit Test Coverage
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: FE Unit Test coverage shows the unit test coverage of our code base.  As
    an example 95% represents that 95% of the LOC in our FE software is unit tested.  It’s
    important as it shows how much code is tested early in the development process.
  target: Above 75%
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - Couple of months custom VM's were cleaned up and it seems that this might have been one, this is currently investigated as our CI doesn't output easy to read percentages.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/SourceCodeKPIs/FrontendJestCoverage
  urls:
  - https://app.periscopedata.com/app/gitlab/475029/Source-Code-KPI's
- name: Project/Area Maintainership Health
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: A project's maintainership is considered unhealthy if it has fewer maintainers than the target maintainer count. Each project's target maintainer count is based on the number of incoming MRs and maintainer availability.
  target: Below 20%
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - The chart shows a decrease in overall unhealthy projects or areas in the last quarter
    - Some areas are not being properly tracked which should lead to a further decrease once resolved
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/MaintainershipDemand/ProjectAreaMaintainershipHealth
- name: Unhealthy Core Areas of Maintainership Health
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Within a given project (for example, gitlab-org/gitlab), maintainers cover different areas within the project - backend, database, frontend, and more. An area of maintainership receives more than 100 merge requests per month and is considered unhealthy if it has less maintainers than the target maintainer count. This indicator is a subset of Project Maintainership Health.
  target: 0
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - Our unhealthy areas have increased within the past 2 months. The unhealthy areas are GitLab backend and database (consistent), but GitLab frontend and Gitaly have now shown up too.
    - Availability also trended down dramatically this last quarter which explains the increase here.
    - Already this month we've been trending back in the right direction.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/MaintainershipDemand/CoreAreasofMaintainershipHealth
- name: Open MR Age (OMA)
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: We want to be more intuitive with calculating how long it takes an MR to merge or close. Open MR Age (OMA) measures the median time of all open MRs as of a specific date. In other words, on any given day, we calculate the number of open MRs and median time in open state for those MRs at that point in time.
  target: At or below 30
  org: Development Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are seeing an overall downwards trend towards the target in the past month.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OpenMRAgeOMA/Sheet1
- name: Review Time to Merge (RTTM)
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: Review Time to Merge (RTTM) tells us on average how long it takes from submitting code for review to being merged. The <a href="https://gitlab.com/clefelhocz1">VP of Development</a> is the DRI on <a href="https://gitlab.com/gitlab-data/analytics/blob/master/transform/snowflake-dbt/data/engineering_productivity_metrics_projects_to_include.csv">what projects are included</a>.
  target: At or below 3
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - This is a revived metric and we are currently monitoring the trends.
  tableau_data:
    charts:
     - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/RTTM/Sheet1
- name: Overall MRs by Type
  base_path: "/handbook/engineering/development/performance-indicators/"
  definition: We want to measure the breakdown of our development investment by MR
    type/label. We only consider MRs that contribute to our product. If an MR has more than one of these labels, the highest one in the
    list takes precedence.
  target: "< 5% change in proportion of MRs with undefined label"
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
    - Have worked to remove undefined MRs.
    - The Engineering Manager for each team is ultimately responsible for ensuring
      that these labels are set correctly.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/MergeRequestMetrics/OverallMRsbyType_1
- name: Development Department Promotion Rate
  base_path: "/handbook/engineering/performance-indicators/"
  definition: The total number of promotions over a rolling 12 month period divided by the month end headcount. The target promotion rate is 12% of the population. This metric definition is taken from the <a href="https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#promotion-rate">People Success Team Member Promotion Rate PI</a>.
  target: 12%
  org: Development Department
  is_key: false
  health:
    level: 3
    reasons:
      - We are coming in line with promotion level goals.
  tableau_data:
    charts:
      - url: https://us-west-2b.online.tableau.com/t/gitlabpublic/views/PeopleAnalyticsHandbookEmbedding/Promo-Q
        filters:
          - field: DPT Modified Department
            value: Development

