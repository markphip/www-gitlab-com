# frozen_string_literal: true

require 'gitlab'
require 'date'
require_relative '../api_retry'

module ReleasePosts
  class Deprecation
    include ApiRetry
    include Helpers

    attr_reader :path, :project_id

    def initialize(path, project_id)
      @path = path
      @project_id = project_id
    end

    def breaking_change
      file_contents["breaking_change"]
    end

    def title
      file_contents["title"]
    end

    def description
      file_contents["body"]
    end

    def date
      DateTime.parse(commit.committed_date).rfc822
    end

    def first_merged
      DateTime.parse(commit.committed_date)
    end

    def announcement_milestone
      file_contents["announcement_milestone"]
    end

    def removal_milestone
      file_contents["removal_milestone"]
    end

    def link
      @link ||= (file_contents["issue_url"] || merge_request.web_url)
    end

    def to_s
      "- [!#{path}](#{link}) #{title}"
    end

    private

    def commit
      @commit ||= begin
        log_info("[#{Time.now}] Fetching commit information for #{path}")
        file_blame_lines = api_retry { Gitlab.get_file_blame(project_id, path, "master") }

        file_blame_lines.min_by { |entry| DateTime.parse(entry.commit.committed_date) }.commit
      end
    end

    def merge_request
      @merge_request ||= begin
        log_info("[#{Time.now}] Fetching MR information for #{path}")
        merge_requests = api_retry { Gitlab.commit_merge_requests(project_id, commit.id) }

        merge_requests.min_by { |entry| DateTime.parse(entry.created_at) }
      end
    end

    def file_contents
      @file_contents ||= begin
        log_info("[#{Time.now}] Fetching file content for #{path}")
        raw_contents = api_retry { Gitlab.file_contents(project_id, path, "master") }

        YAML.safe_load(raw_contents, permitted_classes: [Date]).first
      end
    end
  end
end
